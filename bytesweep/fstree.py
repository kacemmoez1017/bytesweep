#!/usr/bin/env python3
import os

def build_fstree(dirnode,analysis_basedir,fid):
	path = dirnode['node']['path']
	(dirpath, dirs, files) = next(os.walk(path))
	branch_path = dirnode['node']['branch_path'].copy()
	branch_path.append(dirnode['node']['fid'])
	for f in files:
		fpath = os.path.join(path,f)
		frelpath = os.path.relpath(fpath,analysis_basedir)
		ftype = 'file'
		fnode = {'type':'file','node':{'name':f,'path':fpath,'relpath':frelpath,'fid':fid,'parent_fid':dirnode['node']['fid'],'branch_path':branch_path}}
		if os.path.islink(fpath):
			fnode['type'] = 'link'
			fnode['node']['link_dst'] = os.readlink(fpath)
		fid += 1
		dirnode['files'].append(fnode)
	for d in dirs:
		dpath = os.path.join(path,d)
		drelpath = os.path.relpath(dpath,analysis_basedir)
		dnode = {'type':'dir','node':{'name':d,'path':dpath,'relpath':drelpath,'fid':fid,'parent_fid':dirnode['node']['fid'],'branch_path':branch_path},'dirs':[],'files':[]}
		if os.path.islink(dpath):
			dnode['type'] = 'link'
			dnode['node']['link_dst'] = os.readlink(dpath)
		fid += 1
		dirnode['dirs'].append(dnode)
		if dnode['type'] != 'link':
			fid = build_fstree(dnode,analysis_basedir,fid)
	return fid

def fstree_iterate(dirnode):
	yield dirnode
	for f in dirnode['files']:
		yield f
	for d in dirnode['dirs']:
		yield from fstree_iterate(d)
